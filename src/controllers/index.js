const CategoriaController = require('./categoriaController');
const MarcaController = require('./marcaController');
const ProdutoController = require('./produtoController');
const VitrineController = require('./vitrineController');

module.exports = {
  Categoria: CategoriaController,
  Marca: MarcaController,
  Produto: ProdutoController,
  Vitrine: VitrineController,
};
