const Controller = require('./controller');

const instance = null;

class ProdutoController extends Controller {
  constructor() {
    super(db.Produto);

    this.models = db.Produto;

    if (instance === null) {
      this.instance = this;
    }
  }
}

module.exports = ProdutoController;
