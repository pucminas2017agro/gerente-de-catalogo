const Controller = require('./controller');

const instance = null;

class MarcaController extends Controller {
  constructor() {
    super(db.Marca);
    
    if (instance === null) {
      this.instance = this;
    }
  }
}

module.exports = MarcaController;
