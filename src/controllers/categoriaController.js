const Controller = require('./controller');

const instance = null;

class CategoriaController extends Controller {
  constructor() {
    super(db.Categoria);

    if (instance === null) {
      this.instance = this;
    }
  }
}

module.exports = CategoriaController;
