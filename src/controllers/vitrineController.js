const Controller = require('./controller');

const instance = null;

class VitrineController extends Controller {
  constructor() {
    super(db.Vitrine);

    if (instance === null) {
      this.instance = this;
    }
  }
}

module.exports = VitrineController;
