class Controller {
  constructor(model) {
    this.model = model;
  }

  create(req, res) {
    return this.model
      .create(req.body)
      .then(response => res.status(201).send(response))
      .catch(error => res.status(400).send(error));
  }

  list(req, res) {
    return this.model
      .findAll(req.query)
      .then(response => res.status(200).send(response))
      .catch(error => res.status(400).send(error));
  }

  retrieve(req, res) {
    return this.model
      .findById(req.params.id)
      .then((response) => {
        if (!response) {
          return res.status(404).send({ message: 'Not Found' });
        }
        return res.status(200).send(response);
      })
      .catch(error => res.status(400).send(error));
  }

  update(req, res) {
    return this.model
      .findById(req.params.id)
      .then((response) => {
        if (!response) {
          return res.status(404).send({ message: 'Not Found' });
        }
        return response
          .update(req.body)
          .then(() => res.status(204).send(response))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  }

  destroy(req, res) {
    return this.model
      .findById(req.params.id)
      .then((response) => {
        if (!response) {
          return res.status(404).send({ message: 'Not Found' });
        }
        return response
          .destroy()
          .then(() => res.status(204).send(response))
          .catch(error => res.status(400).send(error));
      })
      .catch(error => res.status(400).send(error));
  }
}

module.exports = Controller;
