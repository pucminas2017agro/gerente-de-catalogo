const express = require('express');
const categoriaRoute = require('./categoriaRoutes');
const marcaRoute = require('./marcaRoutes');
const produtoRoute = require('./produtoRoutes');
const vitrineRoute = require('./vitrineRoutes');

const router = express.Router();

router.use('/categoria', categoriaRoute);
router.use('/marca', marcaRoute);
router.use('/produto', produtoRoute);
router.use('/vitrine', vitrineRoute);

module.exports = router;
