const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Vitrine', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    nome: {
      type: DataTypes.STRING(45),
      allowNull: false,
    },
    id_produto: {
      type: DataTypes.UUID,
      references: {
        model: 'produto',
        key: 'id',
      },
    },
  }, {
    tableName: 'vitrine',
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_atualizacao',
  });

  Model.associate = (models) => {
    console.log(`VitrineAssociate: ${models}`);
  }


  return Model;
}
