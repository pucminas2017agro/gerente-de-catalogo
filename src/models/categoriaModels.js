module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Categoria', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    id_categoria_pai: {
      type: DataTypes.UUID,
      references: {
        model: 'categoria',
        key: 'id',
      },
    },
    descricao: {
      type: DataTypes.STRING(50),
      allowNull: false,
    },
  }, {
    tableName: 'categoria',
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_atualizacao',
  });

  Model.associate = (models) => {
    console.log(`CategoriaAssociate: ${models}`);
  }

  return Model;

};
