module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Marca', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    descricao: {
      type: DataTypes.STRING(20),
      allowNull: false,
    },
  }, {
    tableName: 'marca',
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_atualizacao',
  });

  Model.associate = (models) => {
    console.log(`MarcaAssociate: ${models}`);
  }

  return Model;

};
