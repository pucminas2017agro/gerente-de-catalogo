const bcrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
  const Model = sequelize.define('Produto', {
    id: {
      type: DataTypes.UUID,
      defaultValue: DataTypes.UUIDV4,
      primaryKey: true
    },
    id_marcas: {
      type: DataTypes.UUID,
      references: {
        model: 'marca',
        key: 'id',
      },
    },
    id_categoria: {
      type: DataTypes.UUID,
      references: {
        model: 'categoria',
        key: 'id',
      },
    },
    nome: {
      type: DataTypes.STRING(45),
      allowNull: false,
    },
    descricao: {
      type: DataTypes.STRING(100),
      allowNull: false,
    },
    largura: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false,
    },
    altura: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false,
    },
    peso: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false,
    },
    valorUnitario: {
      type: DataTypes.DECIMAL(10,2),
      allowNull: false,
    },
    dataFabricacao: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    dataValidade: {
      type: DataTypes.DATE,
      allowNull: false,
    },
    fotos: {
      type: DataTypes.JSON,
      allowNull: false,
    },
    promocao: {
      type: DataTypes.JSON,
      allowNull: true,
    }
  }, {
    tableName: 'produto',
    freezeTableName: true,
    timestamps: true,
    createdAt: 'data_criacao',
    updatedAt: 'data_atualizacao',
  });

  Model.associate = (models) => {
    console.log(`ProdutoAssociate: ${models}`);
  }

  return Model;
}
