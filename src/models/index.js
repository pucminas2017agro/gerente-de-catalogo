const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const basename = path.basename(module.filename);
const env = process.env.NODE_ENV || 'development';
const config = require('../config/config.json')[env];

let db = null;

module.exports = () => {
  if(!db) {
    db = {};
    let sequelize;
    
    if (config.use_env_variable) {
      sequelize = new Sequelize(process.env[config.use_env_variable]);
    } else {
      sequelize = new Sequelize(config.database, config.username, config.password, config);
    }
    
    fs
      .readdirSync(__dirname)
      .filter(file => (file.indexOf('.') !== 0 && file !== basename && file.slice(-3) === '.js'))
      .forEach((file) => {
        const model = sequelize.import(path.join(__dirname, file));
        db[model.name] = model;
      });

    Object.keys(db).forEach((modelName) => {
      if (db[modelName].associate) {
        db[modelName].associate(db[modelName])
      }
    });

    db.sequelize = sequelize;
    db.Sequelize = Sequelize;
    
    sequelize
      // .sync()
      .sync({force: true})
      .then(() => {
        db.Categoria.bulkCreate([
            { id: 'e5a93f24-ce31-45f5-9d08-55a3fb8e3001', descricao: 'Produtos Veterinários'},
            { id: 'e5a93f24-ce31-45f5-9d08-55a3fb8e3002', descricao: 'Fazenda'},
            { id: 'e5a93f24-ce31-45f5-9d08-55a3fb8e3003', descricao: 'Casa e Jardim'},
            { id: 'e5a93f24-ce31-45f5-9d08-55a3fb8e3004', descricao: 'PET Cães e Gatos'},
            { id: 'e5a93f24-ce31-45f5-9d08-55a3fb8e3005', descricao: 'Pragas Urbanas'},
            { id: 'e5a93f24-ce31-45f5-9d08-55a3fb8e3006', descricao: 'Selaria e Montaria'},
            { descricao: 'Rações', id_categoria_pai: 'e5a93f24-ce31-45f5-9d08-55a3fb8e3004'},
            { descricao: 'Suplementos', id_categoria_pai: 'e5a93f24-ce31-45f5-9d08-55a3fb8e3004'},
        ]);
      })
      .done(() => db)
  }

  return db;
}
  