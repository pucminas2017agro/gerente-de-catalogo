const bodyParser = require('body-parser');
const compression = require('compression');
const cors = require('cors');
const express = require('express');
const helmet = require('helmet');
const logger = require('morgan');
const methodOverride = require('method-override');
const database = require('./src/models/index');

global.db = database();

const v1 = require('./src/routes/v1');

const app = express();
app.set('json spaces', 4);
app.use(logger('dev'));
app.use(helmet());
app.use(cors());
app.use(compression());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

app.use('/v1', v1);

app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the beginning of nothingness.',
}));

module.exports = app;
